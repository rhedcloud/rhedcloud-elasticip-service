####build and start service locally using local tomcat, install and use dev envivironment including appConfig,sample message and EOs
####  uncomment ./gen-webservice elasticip to generate ws
git pull
mvn package

echo  ************Preparing tomcat and axis2...***************
if [ ! -d 'apache-tomcat-8.0.50' ]; then
    wget  https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.50/bin/apache-tomcat-8.0.50.tar.gz
    tar xvzf apache-tomcat-8.0.50.tar.gz
fi


if [ ! -d 'openeai-servicegen' ]; then
    git clone git@bitbucket.org:itarch/openeai-servicegen.git
fi
cp deploy/build-test/servicegen-configs/elasticip.properties openeai-servicegen/properties
cd openeai-servicegen
./gen-webservice elasticip
cd ..

mkdir -p deploy/esb-dev/libs/Axis2
cp openeai-servicegen/target/elasticip/emory-elasticip-webservice/build/lib/emory-elasticip-webservice-1.0-wsdl-classes.jar deploy/build-test/libs/ElasticIpService/emory-elasticip-webservice-1.0-wsdl-classes.jar
cp openeai-servicegen/target/elasticip/emory-elasticip-webservice/build/lib/emory-elasticip-webservice-1.0-localhost.aar deploy/esb-dev/libs/Axis2/emory-elasticip-webservice-1.0.aar

cd deploy/esb-dev/libs/Axis2
unzip -o ../../../../resources/axis2-1.5.2-war.zip axis2.war
mkdir -p WEB-INF/services
mkdir -p WEB-INF/lib
mkdir -p WEB-INF/classes
mkdir -p WEB-INF/conf
cp ../../../../lib/*.jar WEB-INF/lib
cp ../../../../lib/aws-moa-master-*.jar WEB-INF/lib
cp ../../../../lib/emory-moa-1.0.0-*.jar WEB-INF/lib
cp ../../../../lib/openeai.jar WEB-INF/lib
cp ../../../../target/*.jar WEB-INF/lib
cp ../../../build-test/libs/ElasticIpService/* WEB-INF/lib
cp ../../../build-test/configs/messaging/Environments/Examples/Jars/ElasticIpService/*.jar WEB-INF/lib
cp -r ../../../build-test/configs/messaging/Environments/Examples/Jars/ElasticIpService/hibernate/* WEB-INF/classes
cp ../../hibernate.cfg.xml WEB-INF/classes
cp emory-elasticip-webservice-1.0.aar WEB-INF/services
jar uf axis2.war WEB-INF/services/emory-elasticip-webservice-1.0.aar
jar uf axis2.war WEB-INF/lib/*
jar uf axis2.war WEB-INF/classes
jar uf axis2.war WEB-INF/conf
#zip -d axis2.war WEB-INF/lib/httpcore-4.0.jar  # conflicts with httpcore-4.4.4.jar from lib folder
rm -Rf WEB-INF

cp axis2.war ../../../../apache-tomcat-8.0.50/webapps
cd ../../../../apache-tomcat-8.0.50/bin
cp ../../deploy/build-test/setenv.sh .
rm logs/*
rm ../logs/*
export TOMCAT_HOME=..
export CATALINA_HOME=..
./startup.sh

sleep 5 
curl http://localhost:8080/axis2/services/ElasticIpService/getVersion
#cp ../axis2/1.5.2 $TOMCAT_HOME/webapps/axis2

