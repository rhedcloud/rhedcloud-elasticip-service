unzip -o $BUILD_HOME/resources/axis2-1.5.2-war.zip axis2.war 
mkdir -p WEB-INF/services
mkdir -p WEB-INF/lib
mkdir -p WEB-INF/classes
cp rhedcloud-elasticip-webservice-1.0.aar WEB-INF/services
cp -r ../../../build-test/libs/ElasticIpService/* WEB-INF/lib       
cp -r ../../../build-test/configs/messaging/Environments/Examples/Jars/ElasticIpService/hibernate/* WEB-INF/classes      
cp ../../hibernate.cfg.xml WEB-INF/classes
jar uf axis2.war WEB-INF/services/rhedcloud-elasticip-webservice-1.0.aar  
jar uf axis2.war WEB-INF/lib   
jar uf axis2.war WEB-INF/classes  
jar tf axis2.war
rm -Rf WEB-INF

