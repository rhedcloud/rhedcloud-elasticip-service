mvn package
echo copying
export TOMCAT_HOME=apache-tomcat-8.0.50
cp target/*.jar $TOMCAT_HOME/webapps/axis2/WEB-INF/lib
cp deploy/build-test/configs/messaging/Environments/Examples/Jars/ElasticIpService/hibernate/querylanguages.xml $TOMCAT_HOME/webapps/axis2/WEB-INF/classes
echo touching
touch $TOMCAT_HOME/webapps/axis2/WEB-INF/web.xml
