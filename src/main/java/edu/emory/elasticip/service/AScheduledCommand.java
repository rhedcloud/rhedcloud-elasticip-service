/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************

 Copyright (C) 2018 Emory University. All rights reserved.
 ******************************************************************************/

package edu.emory.elasticip.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openeai.afa.ScheduledCommand;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.afa.ScheduledCommandImpl;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.provider.ProviderException;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;
import org.openeai.utils.webservice.command.OpeneaiCommand;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloud;
import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudQuerySpecification;
import com.service_now.moa.jmsobjects.servicedesk.v2_0.Incident;
import com.service_now.moa.objects.resources.v2_0.IncidentRequisition;

import edu.emory.moa.jmsobjects.network.v1_0.ElasticIpAssignment;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNatDeprovisioning;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNatProvisioning;
import edu.emory.moa.objects.resources.v1_0.ElasticIpAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatDeprovisioningQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatProvisioningQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;

/**
 *TODO: to be deleted? moved to NetworkOpsService
 *
 * @author George Wang (gwang28@emory.edu)
 * @version 1.0 - 4 July 2018
 *
 */
public abstract class AScheduledCommand extends ScheduledCommandImpl implements ScheduledCommand {
    private String LOGTAG = "[AScheduledCommand] ";
    protected static final Logger logger = LogManager.getLogger(AScheduledCommand.class);
    protected boolean m_verbose = false;
    protected static int requestTimeoutIntervalMilli = -1;
    protected ProducerPool elasticIpServiceRequestProducerPool;
    protected ProducerPool networkOpsServiceRequestProducerPool;
    protected ProducerPool serviceNowServiceRequestProducerPool;
    protected ProducerPool awsAccountServiceRequestProducerPool;

    protected String elasticIpRequestUserNetID = "webservice_admin_integration";
    protected String incidentRequisitionCallerId = "webservice_admin_integration";
    protected String staticNatProvisioningRequestCommand = "StaticNatProvisioningRequestCommand";
    protected String staticNatDeprovisioningRequestCommand = "StaticNatDeprovisioningRequestCommand";
    protected float staticNatProvisioningMaxWaitInMinutes = 1;
    protected float staticNatDeprovisioningMaxWaitInMinutes = 1;
    protected int staticNatPollingIntervalInSeconds = 5;
    protected String rdbmsRequestCommand = "RdbmsRequestCommand";
    protected StaticNatProvisioning staticNatProvisioningTemplate;
    protected StaticNatDeprovisioning staticNatDeprovisioningTemplate;
    protected StaticNat staticNatTemplate;
    protected ElasticIpAssignment elasticIpAssignmentTemplate;
    public AScheduledCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + " Initializing ...");

        // Get the command properties
        PropertyConfig pConfig = new PropertyConfig();
        try {

            elasticIpServiceRequestProducerPool = (ProducerPool) getAppConfig().getObject("ElasticIpServiceRequestProducer");
            networkOpsServiceRequestProducerPool = (ProducerPool) getAppConfig().getObject("NetworkOpsServiceRequestProducer");
            serviceNowServiceRequestProducerPool = (ProducerPool) getAppConfig().getObject("ServiceNowServiceRequestProducer");
            awsAccountServiceRequestProducerPool = (ProducerPool) getAppConfig().getObject("AwsAccountServiceRequestProducer");

            String staticNatProvisioningMaxWaitInMinutesStr = pConfig.getProperties().getProperty("staticNatProvisioningMaxWaitInMinutes",
                    "1");
            staticNatProvisioningMaxWaitInMinutes = Float.parseFloat(staticNatProvisioningMaxWaitInMinutesStr);
            String staticNatDeprovisioningMaxWaitInMinutesStr = pConfig.getProperties()
                    .getProperty("staticNatDeprovisioningMaxWaitInMinutes", "1");
            staticNatDeprovisioningMaxWaitInMinutes = Float.parseFloat(staticNatDeprovisioningMaxWaitInMinutesStr);

            String staticNatPollingIntervalInSecondsStr = pConfig.getProperties().getProperty("staticNatPollingIntervalInSeconds", "5");
            staticNatPollingIntervalInSeconds = Integer.parseInt(staticNatPollingIntervalInSecondsStr);

            staticNatProvisioningTemplate = (StaticNatProvisioning) getAppConfig().getObjectByType(StaticNatProvisioning.class.getName());
            staticNatDeprovisioningTemplate = (StaticNatDeprovisioning) getAppConfig()
                    .getObjectByType(StaticNatDeprovisioning.class.getName());

            staticNatTemplate = (StaticNat) getAppConfig().getObjectByType(StaticNat.class.getName());
            elasticIpAssignmentTemplate = (ElasticIpAssignment) getAppConfig().getObjectByType(ElasticIpAssignment.class.getName());
            pConfig = (PropertyConfig) getAppConfig().getObject("GeneralProperties");
            Properties props = pConfig.getProperties();
            setProperties(props);
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving a PropertyConfig object from " + "AppConfig: The exception is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // Get the verbose property.
        String verbose = getProperties().getProperty("verbose", "false");
        setVerbose(Boolean.getBoolean(verbose));
        logger.info(LOGTAG + "property verbose: " + getVerbose());

        logger.info(LOGTAG + " Initialization complete.");

        elasticIpRequestUserNetID = pConfig.getProperties().getProperty("elasticIpRequestUserNetID", "webservice_admin_integration");
        incidentRequisitionCallerId = pConfig.getProperties().getProperty("incidentRequisitionCallerId", "webservice_admin_integration");
        staticNatProvisioningRequestCommand = pConfig.getProperties().getProperty("staticNatProvisioningRequestCommand",
                "StaticNatProvisioningRequestCommand");
        staticNatDeprovisioningRequestCommand = pConfig.getProperties().getProperty("staticNatDeprovisioningRequestCommand",
                "StaticNatDeprovisioningRequestCommand");
        rdbmsRequestCommand = pConfig.getProperties().getProperty("rdbmsRequestCommand", "RdbmsRequestCommand");

    }

    protected MessageProducer getRequestServiceMessageProducer(ProducerPool producerPool) {
        MessageProducer producer = null;
        try {
            producer = producerPool.getExclusiveProducer();
            if (producer instanceof PointToPointProducer) {
                PointToPointProducer p2pp = (PointToPointProducer) producer;
                if (requestTimeoutIntervalMilli != -1) {
                    p2pp.setRequestTimeoutInterval(requestTimeoutIntervalMilli);
                    if (getVerbose())
                        logger.info(LOGTAG + "p2pProducer.setRequestTimeoutInterval=" + requestTimeoutIntervalMilli);
                }
            }
        } catch (JMSException jmse) {
            String errMsg = "An error occurred getting a request service. The " + "exception is: " + jmse.getMessage();
            logger.fatal(LOGTAG + errMsg, jmse);
            throw new java.lang.UnsupportedOperationException(errMsg, jmse);
        }
        return producer;
    }

    protected String queryVpcAccountIdFromAwsAccountService(String ownerId)
            throws EnterpriseConfigurationObjectException, EnterpriseObjectQueryException, EnterpriseFieldException {
        VirtualPrivateCloud virtualPrivateCloud = (VirtualPrivateCloud) getAppConfig().getObjectByType(VirtualPrivateCloud.class.getName());
        VirtualPrivateCloudQuerySpecification virtualPrivateCloudQuerySpecification = (VirtualPrivateCloudQuerySpecification) getAppConfig()
                .getObjectByType(VirtualPrivateCloudQuerySpecification.class.getName());
        virtualPrivateCloudQuerySpecification.setVpcId(ownerId);
        MessageProducer awsAccountProducer = getRequestServiceMessageProducer(awsAccountServiceRequestProducerPool);
        // TODO: from ipAddress (public or private?) to cidr?
        // TODO: query vpc by ipAddress?
        // virtualPrivateCloudQuerySpecification.set
        // RdmsRequestCommand is defaultCommand
        List<VirtualPrivateCloud> virtualPrivateClouds = virtualPrivateCloud.query(virtualPrivateCloudQuerySpecification,
                (RequestService) awsAccountProducer);
        return virtualPrivateClouds.get(0).getAccountId();
    }

    @Override
    public abstract int execute() throws ScheduledCommandException;
    protected boolean processStaticNatProvisioning(String elasticIpAddress, String associatedIpAddressTo)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectGenerateException,
            XmlEnterpriseObjectException, EnterpriseObjectQueryException, InterruptedException, ProviderException {
        StaticNatProvisioning staticNatProvisioning = staticNatProvinsioningGenerate(elasticIpAddress, associatedIpAddressTo);
        StaticNatProvisioningQuerySpecification staticNatProvisioningQuerySpecification = (StaticNatProvisioningQuerySpecification) getAppConfig()
                .getObjectByType(StaticNatProvisioningQuerySpecification.class.getName());
        staticNatProvisioningQuerySpecification.setProvisioningId(staticNatProvisioning.getProvisioningId());
        long startTime = System.currentTimeMillis();
        long staticNatProvisioninTimeUpTime = startTime + (long) (staticNatProvisioningMaxWaitInMinutes * 60 * 1000);
        while (System.currentTimeMillis() < staticNatProvisioninTimeUpTime
                && !"completed".equalsIgnoreCase(staticNatProvisioning.getStatus())) {
            logger.warn(LOGTAG + "CurrentStatus:" + staticNatProvisioning.getStatus() + ",provisioningResult:"
                    + staticNatProvisioning.getProvisioningResult() + ".Sleeping in seconds:" + staticNatPollingIntervalInSeconds + "...");
            Thread.sleep(staticNatPollingIntervalInSeconds * 1000);
            staticNatProvisioning = queryStaticNatProvisioning(staticNatProvisioningQuerySpecification);
        }
        return "success".equalsIgnoreCase(staticNatProvisioning.getProvisioningResult());

    }
    protected StaticNatProvisioning queryStaticNatProvisioning(
            StaticNatProvisioningQuerySpecification staticNatProvisioningQuerySpecification) throws EnterpriseConfigurationObjectException,
            EnterpriseFieldException, EnterpriseObjectQueryException, EnterpriseObjectGenerateException {
        List<XmlEnterpriseObject> staticNatProvisioningsReturned;
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        try {
            logger.info(
                    "querying staticNatProvisioning with provisioningId:" + staticNatProvisioningQuerySpecification.getProvisioningId());
            staticNatProvisioningsReturned = OpeneaiCommand.QueryCommand().exec(staticNatProvisioningTemplate,
                    (RequestService) networkOpsProducer, staticNatProvisioningQuerySpecification);
            logger.info("statusReturned:" + ((StaticNatProvisioning) staticNatProvisioningsReturned.get(0)).getStatus());
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        return (StaticNatProvisioning) staticNatProvisioningsReturned.get(0);
    }
    protected StaticNatProvisioning staticNatProvinsioningGenerate(String publicIpAddress, String associatedIpAddressTo)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectGenerateException,
            XmlEnterpriseObjectException {
        StaticNatProvisioning staticNatProvisioning = (StaticNatProvisioning) getAppConfig()
                .getObjectByType(StaticNatProvisioning.class.getName());
        StaticNat staticNat = (StaticNat) getAppConfig().getObjectByType(StaticNat.class.getName());
        staticNat.setPublicIp(publicIpAddress);
        staticNat.setPrivateIp(associatedIpAddressTo);
        staticNatProvisioning.setCommandName(staticNatProvisioningRequestCommand);
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        List<StaticNatProvisioning> staticNatProvisioningsReturned = new ArrayList<>();
        try {
            logger.info(LOGTAG + "StaticNatProvisioning.generate:staticNat=" + staticNat.toXmlString());
            staticNatProvisioningsReturned = staticNatProvisioning.generate(staticNat, (RequestService) networkOpsProducer);
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        logger.info(LOGTAG + "staticNatProvisioningsReturnedt.generateReturn.size=" + staticNatProvisioningsReturned.size());
        logger.info(LOGTAG + "staticNatProvisioningsReturnedt.generateReturn[0]=" + staticNatProvisioningsReturned.get(0).toXmlString());
        return staticNatProvisioningsReturned.get(0);
    }

    protected boolean processStaticNatDeprovisioning(String elasticIpAddress, String associatedIpAddress)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, XmlEnterpriseObjectException,
            EnterpriseObjectGenerateException, EnterpriseObjectQueryException, InterruptedException, ProviderException {
        StaticNatDeprovisioning staticNatDeprovisioning = staticNatDeprovisioningGenerate(elasticIpAddress, associatedIpAddress);
        StaticNatDeprovisioningQuerySpecification staticNatDeprovisioningQuerySpecification = (StaticNatDeprovisioningQuerySpecification) getAppConfig()
                .getObjectByType(StaticNatDeprovisioningQuerySpecification.class.getName());
        staticNatDeprovisioningQuerySpecification.setProvisioningId(staticNatDeprovisioning.getProvisioningId());
        long startTime = System.currentTimeMillis();
        long staticNatDeprovisioninTimeUpTime = startTime + (long) (staticNatDeprovisioningMaxWaitInMinutes * 60 * 1000);
        while (System.currentTimeMillis() < staticNatDeprovisioninTimeUpTime
                && !"completed".equalsIgnoreCase(staticNatDeprovisioning.getStatus())) {
            logger.warn(LOGTAG + "CurrentStatus:" + staticNatDeprovisioning.getStatus() + ",provisioningResult:"
                    + staticNatDeprovisioning.getProvisioningResult() + ".Sleeping in seconds:" + staticNatPollingIntervalInSeconds
                    + "...");
            Thread.sleep(staticNatPollingIntervalInSeconds * 1000);
            staticNatDeprovisioning = queryStaticNatDeprovisioning(staticNatDeprovisioningQuerySpecification);
        }
        return "success".equalsIgnoreCase(staticNatDeprovisioning.getProvisioningResult());
    }

    protected StaticNatDeprovisioning staticNatDeprovisioningGenerate(String elasticIpAddress, String associatedIpAddress)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, XmlEnterpriseObjectException,
            EnterpriseObjectGenerateException {
        StaticNatDeprovisioning staticNatDeprovisioningTemplate = (StaticNatDeprovisioning) getAppConfig()
                .getObjectByType(StaticNatDeprovisioning.class.getName());
        StaticNat staticNat = (StaticNat) getAppConfig().getObjectByType(StaticNat.class.getName());
        staticNat.setPublicIp(elasticIpAddress);
        staticNat.setPrivateIp(associatedIpAddress);
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        staticNatDeprovisioningTemplate.setCommandName(staticNatDeprovisioningRequestCommand);
        List<StaticNatDeprovisioning> staticNatDeprovisioningsReturned = new ArrayList<>();
        try {
            staticNatDeprovisioningsReturned = staticNatDeprovisioningTemplate.generate(staticNat, (RequestService) networkOpsProducer);
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        logger.info(LOGTAG + "staticNatDeprovisioning.generateReturn.size=" + staticNatDeprovisioningsReturned.size());
        logger.info(LOGTAG + "staticNatDeprovisioning.generateReturn[0]=" + staticNatDeprovisioningsReturned.get(0).toXmlString());
        return staticNatDeprovisioningsReturned.get(0);
    }
    private StaticNatDeprovisioning queryStaticNatDeprovisioning(
            StaticNatDeprovisioningQuerySpecification staticNatDeprovisioningQuerySpecification)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectQueryException,
            EnterpriseObjectGenerateException {
        List<XmlEnterpriseObject> staticNatDeprovisioningsReturned;
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        try {
            logger.info("querying staticNatDeprovisioning with provisioningId:"
                    + staticNatDeprovisioningQuerySpecification.getProvisioningId());
            staticNatDeprovisioningsReturned = OpeneaiCommand.QueryCommand().exec(staticNatDeprovisioningTemplate,
                    (RequestService) networkOpsProducer, staticNatDeprovisioningQuerySpecification);
            logger.info("statusReturned:" + ((StaticNatDeprovisioning) staticNatDeprovisioningsReturned.get(0)).getStatus());
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        return (StaticNatDeprovisioning) staticNatDeprovisioningsReturned.get(0);
    }

    protected Incident serviceNowIncidentGenerate(String shorDescription, String description)
            throws org.openeai.jms.consumer.commands.provider.ProviderException {
        List<Incident> generatedIncidents = null;
        try {
            Incident incident = (Incident) getAppConfig().getObjectByType(Incident.class.getName());
            IncidentRequisition incidentRequisition = (IncidentRequisition) getAppConfig()
                    .getObjectByType(IncidentRequisition.class.getName());
            incidentRequisition.setShortDescription(shorDescription);
            incidentRequisition.setDescription(description);
            incidentRequisition.setUrgency("3");
            incidentRequisition.setImpact("3");
            incidentRequisition.setBusinessService("Application Management");
            incidentRequisition.setCategory("Configuration");
            incidentRequisition.setSubCategory("Add");
            incidentRequisition.setRecordType("Service Request");
            incidentRequisition.setContactType("Integration");
            incidentRequisition.setCallerId(incidentRequisitionCallerId);
            incidentRequisition.setCmdbCi("Emory AWS Service");
            incidentRequisition.setAssignmentGroup("LITS: Messaging - Tier 3");
            logger.info("incident.generate:incidentRequisition=" + incidentRequisition.toXmlString());
            MessageProducer serviceNowProducer = getRequestServiceMessageProducer(serviceNowServiceRequestProducerPool);
            try {
                generatedIncidents = incident.generate(incidentRequisition, (RequestService) serviceNowProducer);
            } finally {
                serviceNowServiceRequestProducerPool.releaseProducer(serviceNowProducer);
            }
        } catch (Throwable e1) {
            logger.error(LOGTAG, e1);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException("ServiceNowIncident.Generate error:" + e1.getMessage());
        }
        return generatedIncidents.get(0);
    }

    /**
     * @param boolean,
     *            the verbose parameter
     *            <P>
     *            Set a parameter to toggle verbose logging.
     */
    protected void setVerbose(boolean b) {
        m_verbose = b;
    }

    /**
     * @return boolean, the verbose parameter
     *         <P>
     *         Gets the value of the verbose logging parameter.
     */
    protected boolean getVerbose() {
        return m_verbose;
    }
}
