/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************

 Copyright (C) 2018 Emory University. All rights reserved.
 ******************************************************************************/

package edu.emory.elasticip.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openeai.afa.ScheduledCommand;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.afa.ScheduledCommandImpl;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.provider.ProviderException;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;
import org.openeai.utils.webservice.command.OpeneaiCommand;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountNotification;
import com.amazon.aws.moa.objects.resources.v1_0.Datetime;

import edu.emory.moa.jmsobjects.network.v1_0.ElasticIpAssignment;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNatDeprovisioning;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNatProvisioning;
import edu.emory.moa.objects.resources.v1_0.ElasticIpAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatDeprovisioningQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatProvisioningQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;

/**
 *TODO: to be deleted? moved to NetworkOpsService
 *
 * @author George Wang (gwang28@emory.edu)
 * @version 1.0 - 4 July 2018
 *
 */
public class NetworkOpsRegistryScheduledCommand extends AScheduledCommand {
    private String LOGTAG = "[ElasticIpRegistryScheduledCommand] ";
    protected static final Logger logger = LogManager.getLogger(NetworkOpsRegistryScheduledCommand.class);
    public NetworkOpsRegistryScheduledCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
    }

    protected List<StaticNat> queryAllStaticNatsForAwsAtEmory() throws org.openeai.jms.consumer.commands.provider.ProviderException {
        // TODO:limit to a speciic range of network addresses provided by Jimmy
        // TODO: put in a filter?
        List<StaticNat> staticNats = new ArrayList<>();
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        try {
            StaticNatQuerySpecification staticNatQuerySpecification = (StaticNatQuerySpecification) getAppConfig()
                    .getObjectByType(StaticNatQuerySpecification.class.getName());
            StaticNat staticNat = (StaticNat) getAppConfig().getObjectByType(StaticNat.class.getName());
            staticNat.setCommandName(rdbmsRequestCommand);
            staticNats = staticNat.query(staticNatQuerySpecification, (RequestService) networkOpsProducer);
        } catch (EnterpriseObjectQueryException | EnterpriseConfigurationObjectException e) {
            logger.error(LOGTAG, e);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(e.getMessage());
        } finally {
            try {
                networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
            } catch (Throwable t) {
            }
        }
        return staticNats;
    }
    @Override
    public int execute() throws ScheduledCommandException {
        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "Executing ...");

        try {
            List<StaticNat> staticNats = queryAllStaticNatsForAwsAtEmory();
            logger.info(LOGTAG + "all staticNats from networkOps: staticNats.size()= " + staticNats.size());
            ElasticIpAssignmentQuerySpecification elasticIpAssignmentQuerySpecification = (ElasticIpAssignmentQuerySpecification) getAppConfig()
                    .getObjectByType(ElasticIpAssignmentQuerySpecification.class.getName());
            for (StaticNat staticNat : staticNats) {
                elasticIpAssignmentTemplate.setCommandName(rdbmsRequestCommand);
                elasticIpAssignmentQuerySpecification.setElasticIpAddress(staticNat.getPublicIp());
                MessageProducer elasticIpProducer = getRequestServiceMessageProducer(elasticIpServiceRequestProducerPool);
                List<ElasticIpAssignment> elasticIpAssignments = null;
                try {
                    elasticIpAssignments = elasticIpAssignmentTemplate.query(elasticIpAssignmentQuerySpecification,
                            (RequestService) elasticIpProducer);
                } finally {
                    elasticIpServiceRequestProducerPool.releaseProducer(elasticIpProducer);
                }
                // since we dont have acountId, we are not sending
                // accountNotification request
                logger.info(LOGTAG + "for staticNat= " + staticNat.toXmlString() + ",elasticIpAssignment=" + elasticIpAssignments == null
                        || elasticIpAssignments.isEmpty() ? null
                                : elasticIpAssignments.get(0).getElasticIp().getElasticIpAddress() + "=>"
                                        + elasticIpAssignments.get(0).getElasticIp().getAssociatedIpAddress());
                if (elasticIpAssignments == null || elasticIpAssignments.isEmpty()) {
                    if (!processStaticNatDeprovisioning(staticNat.getPublicIp(), staticNat.getPrivateIp())) {
                        String text = "StaticNatDeprovisioning:" + staticNat.getPublicIp() + "=>" + staticNat.getPrivateIp();
                        serviceNowIncidentGenerate(text, "failed");
                    }
                    // String accountId =
                    // queryVpcAccountIdFromAwsAccountService(staticNat.getPublicIp());
                    // AccountNotification AccountNotification =
                    // newAccountNotification(accountId);
                    // String text = "StaticNatDeprovisioning:" +
                    // staticNat.getPublicIp() + "=>" +
                    // staticNat.getPrivateIp();
                    // if
                    // (processStaticNatDeprovisioning(staticNat.getPublicIp(),
                    // staticNat.getPrivateIp())) {
                    // AccountNotification.setText(text + ":successful");
                    // } else {
                    // AccountNotification.setText(text + ":failed");
                    // serviceNowIncidentGenerate(text, "failed");
                    // }
                    // MessageProducer awsAccountProducer =
                    // getRequestServiceMessageProducer(awsAccountServiceRequestProducerPool);
                    // AccountNotification.create((RequestService)
                    // awsAccountProducer);
                }
            }
        } catch (ProviderException | EnterpriseConfigurationObjectException | EnterpriseFieldException | EnterpriseObjectQueryException
                | EnterpriseObjectGenerateException | XmlEnterpriseObjectException | InterruptedException e1) {
            logger.info(LOGTAG, e1);
        }
        return 0;
    }

    private AccountNotification newAccountNotification(String accountId)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException {
        AccountNotification AccountNotification = (AccountNotification) getAppConfig().getObjectByType(AccountNotification.class.getName());
        AccountNotification.setAccountId(accountId);
        AccountNotification.setType("ElasticIp");
        AccountNotification.setSubject(" StaticNatDeprovisioning remediation");
        AccountNotification.setCreateUser("ElasticIpService");
        AccountNotification.setCreateDatetime(new Datetime());
        return AccountNotification;
    }
}
