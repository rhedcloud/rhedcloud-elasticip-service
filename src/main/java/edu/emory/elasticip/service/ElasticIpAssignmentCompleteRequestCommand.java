/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved. 
 ******************************************************************************/

package edu.emory.elasticip.service;

// OpenEAI foundation components
import org.openeai.config.CommandConfig;
import org.openeai.jms.consumer.commands.GenericCompleteRequestCommand;
import org.openeai.jms.consumer.commands.RequestCommand;

import edu.emory.elasticip.service.provider.EmoryElasticIpAssignmentCompleteProvider;
import edu.emory.moa.jmsobjects.network.v1_0.ElasticIpAssignment;
import edu.emory.moa.objects.resources.v1_0.ElasticIpAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.ElasticIpRequisition;

public class ElasticIpAssignmentCompleteRequestCommand extends
        GenericCompleteRequestCommand<ElasticIpAssignment, ElasticIpAssignmentQuerySpecification, ElasticIpRequisition, EmoryElasticIpAssignmentCompleteProvider>
        implements RequestCommand {
    public ElasticIpAssignmentCompleteRequestCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
    }
}