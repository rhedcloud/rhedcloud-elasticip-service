/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************
 This file is part of the Emory AWS Account Service.

 Copyright (C) 2016 Emory University. All rights reserved.
 ******************************************************************************/

package edu.emory.elasticip.service.provider;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.provider.AbstractCompleteProvider;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.EnterpriseObjectDeleteException;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.EnterpriseObjectUpdateException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.moa.objects.resources.Result;
import org.openeai.transport.RequestService;
import org.openeai.utils.webservice.command.OpeneaiCommand;

import com.service_now.moa.jmsobjects.customrequests.v1_0.ElasticIpRequest;
import com.service_now.moa.jmsobjects.servicedesk.v2_0.Incident;
import com.service_now.moa.objects.resources.v2_0.IncidentRequisition;

import edu.emory.moa.jmsobjects.network.v1_0.ElasticIp;
import edu.emory.moa.jmsobjects.network.v1_0.ElasticIpAssignment;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNatDeprovisioning;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNatProvisioning;
import edu.emory.moa.objects.resources.v1_0.Datetime;
import edu.emory.moa.objects.resources.v1_0.ElasticIpAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.ElasticIpQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.ElasticIpRequisition;
import edu.emory.moa.objects.resources.v1_0.StaticNatDeprovisioningQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatProvisioningQuerySpecification;

/**
 *
 *
 *
 *
 * 1. If I remember correctly, I think this all happens with
 * ElasticIpAssignment.Update-Request and ElasticIpAssignment.Delete-Request.
 * Basically this is when someone associates a private IP with a public IP or
 * disassociates a private IP with a public IP.
 *
 * 2. What I typically do in cases like this is I write a custom command and
 * provider that handles all actions for the object and then it becomes the
 * command all clients invoke and this command/provider invokes the internal
 * RDBMS command for all basic persistent operations.
 *
 * 3. For the Update-Request I believe what now has to happen is:
 *
 * a.If the associated IP is going from null to a value,
 *
 * i. send a StaticNat.Create-Request to the NetworkOpsService
 *
 * ii. Send an ElasticIpAssignment.Update-Request to the RdbmsRequest command to
 * update the object in the ElasticIpService database
 *
 * iii. Send an ElasticIpRequest.Create-Request ServiceNow to record that an
 * ElasticIp was requested and provisioned
 *
 * iv. If any of this fails send an Incident.Create-Request to ServiceNow and
 * respond to the requestor with an error
 *
 * b. If the associated IP is going from a value to another value, i. send
 * aStaticNatDeprovisioning.generate to the NetworkOpsService to remove the
 * existing NAT
 *
 * ii. send a StaticNat.Create-DeleteRequest to the NetworkOpsService create the
 * new NAT=
 *
 * iii. Send an ElasticIpAssignment.Update-Request to the RdbmsRequest command
 * to update the object in the ElasticIpService database
 *
 * iv. Send an ElasticIpRequest.Create-Request ServiceNow to record that an
 * ElasticIp was requested and provisioned
 *
 * v. If any of this fails send an Incident.Create-Request to ServiceNow and
 * respond to the requestor with an error
 *
 * 4. For the Delete-Request I believe what now has to happen is: If the
 * associated IP is not null, send a StaticNatDeprovisioning.generate to the
 * NetworkOpsService Send an ElasticIpAssignment.Delete-Request to the
 * RdbmsRequest command to delete the object in the ElasticIpService database If
 * an StaticNat was deleted in step a, send an ElasticIpRequest.Create-Request
 * ServiceNow to record that an ElasticIp was deprovisioned If any of this fails
 * send an Incident.Create-Request to ServiceNow and respond to the requestor
 * with an error
 *
 *
 * whenever we create or delete these across both routers, we do a provisigning
 * or deprovisioning generate.
 *
 * That means you need to send the generate and you will get an immediate
 * response with the provisioning or deprovisioning object. Then you need to
 * query for the status of the provisioning object until it is complete and
 * check for a result of success of failure. StaticNat is pretty fast on both
 * routers, so you likely won't cycle very long. I have to wait pretty long for
 * VpnConnectionProvisioning and VpcProvisioning, but StaticNatProvisioning
 * should go pretty fast unless there are other pending requests being handled
 * by the routers. You'll probably want to configure a maximum time to wait for
 * this status to return and just keep checking for that period of time.
 *
 * @author George Wang (gw2100@gmail.com)
 *
 */

public class EmoryElasticIpAssignmentCompleteProvider
        extends AbstractCompleteProvider<ElasticIpAssignment, ElasticIpAssignmentQuerySpecification, ElasticIpRequisition> {
    private static Logger logger = LogManager.getLogger(EmoryElasticIpAssignmentCompleteProvider.class);
    private AppConfig appConfig;
    private String LOGTAG = "[EmoryElasticIpAssignmentCompleteProvider] ";
    private static int requestTimeoutIntervalMilli = -1;
    private boolean verbose = true;
    // needs to match what's in querylanguages.xml
    private static final String QUERY_LANGUAGE_FIND_UNASSIGNED = "findUnassigned";

    private ProducerPool elasticIpServiceRequestProducerPool;
    private ProducerPool networkOpsServiceRequestProducerPool;
    private ProducerPool serviceNowServiceRequestProducerPool;

    private String elasticIpRequestUserNetID = "webservice_admin_integration";
    private String incidentRequisitionCallerId = "webservice_admin_integration";
    private String staticNatProvisioningRequestCommand = "StaticNatProvisioningRequestCommand";
    private String staticNatDeprovisioningRequestCommand = "StaticNatDeprovisioningRequestCommand";
    private float staticNatProvisioningMaxWaitInMinutes = 5;
    private float staticNatDeprovisioningMaxWaitInMinutes = 5;
    private int staticNatPollingIntervalInSeconds = 15;

    private String rdbmsRequestCommand = "RdbmsRequestCommand";

    private StaticNatProvisioning staticNatProvisioningTemplate;
    private StaticNatDeprovisioning staticNatDeprovisioningTemplate;

    @Override
    public void init(AppConfig aConfig) throws org.openeai.jms.consumer.commands.provider.ProviderException {
        super.init(aConfig);
        logger.info(LOGTAG + "Initializing...");
        appConfig = aConfig;
        PropertyConfig pConfig = new PropertyConfig();
        try {
            elasticIpServiceRequestProducerPool = (ProducerPool) appConfig.getObject("ElasticIpServiceRequestProducer");
            networkOpsServiceRequestProducerPool = (ProducerPool) appConfig.getObject("NetworkOpsServiceRequestProducer");
            serviceNowServiceRequestProducerPool = (ProducerPool) appConfig.getObject("ServiceNowServiceRequestProducer");
            pConfig = (PropertyConfig) aConfig.getObject("ProviderProperties");
            elasticIpRequestUserNetID = pConfig.getProperties().getProperty("elasticIpRequestUserNetID", "webservice_admin_integration");
            incidentRequisitionCallerId = pConfig.getProperties().getProperty("incidentRequisitionCallerId",
                    "webservice_admin_integration");
            staticNatProvisioningRequestCommand = pConfig.getProperties().getProperty("staticNatProvisioningRequestCommand",
                    "StaticNatProvisioningRequestCommand");
            staticNatDeprovisioningRequestCommand = pConfig.getProperties().getProperty("staticNatDeprovisioningRequestCommand",
                    "StaticNatDeprovisioningRequestCommand");
            rdbmsRequestCommand = pConfig.getProperties().getProperty("rdbmsRequestCommand", "RdbmsRequestCommand");

            String staticNatProvisioningMaxWaitInMinutesStr = pConfig.getProperties().getProperty("staticNatProvisioningMaxWaitInMinutes",
                    "5");
            staticNatProvisioningMaxWaitInMinutes = Float.parseFloat(staticNatProvisioningMaxWaitInMinutesStr);
            String staticNatDeprovisioningMaxWaitInMinutesStr = pConfig.getProperties()
                    .getProperty("staticNatDeprovisioningMaxWaitInMinutes", "30");
            staticNatDeprovisioningMaxWaitInMinutes = Float.parseFloat(staticNatDeprovisioningMaxWaitInMinutesStr);

            String staticNatPollingIntervalInSecondsStr = pConfig.getProperties().getProperty("staticNatPollingIntervalInSeconds", "15");
            staticNatPollingIntervalInSeconds = Integer.parseInt(staticNatPollingIntervalInSecondsStr);

            staticNatProvisioningTemplate = (StaticNatProvisioning) appConfig.getObjectByType(StaticNatProvisioning.class.getName());
            staticNatDeprovisioningTemplate = (StaticNatDeprovisioning) appConfig.getObjectByType(StaticNatDeprovisioning.class.getName());
        } catch (EnterpriseConfigurationObjectException eoce) {
            String errMsg = "Error retrieving a PropertyConfig object from " + "AppConfig: The exception is: " + eoce.getMessage();
            logger.error(LOGTAG + errMsg);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(errMsg, eoce);
        }
        logger.info(LOGTAG + pConfig.getProperties().toString());
        logger.info(LOGTAG + "Initialization complete.");
    }

    private MessageProducer getRequestServiceMessageProducer(ProducerPool producerPool) {
        MessageProducer producer = null;
        try {
            producer = producerPool.getExclusiveProducer();
            if (producer instanceof PointToPointProducer) {
                PointToPointProducer p2pp = (PointToPointProducer) producer;
                if (requestTimeoutIntervalMilli != -1) {
                    p2pp.setRequestTimeoutInterval(requestTimeoutIntervalMilli);
                    if (verbose)
                        logger.info(LOGTAG + "p2pProducer.setRequestTimeoutInterval=" + requestTimeoutIntervalMilli);
                }
            }
        } catch (JMSException jmse) {
            String errMsg = "An error occurred getting a request service. The " + "exception is: " + jmse.getMessage();
            logger.fatal(LOGTAG + errMsg, jmse);
            throw new java.lang.UnsupportedOperationException(errMsg, jmse);
        }
        return producer;
    }

    @Override
    public void update(ElasticIpAssignment elasticIpAssignment) throws org.openeai.jms.consumer.commands.provider.ProviderException {
        ElasticIpAssignment elasticIpAssignmentBaseline = (ElasticIpAssignment) elasticIpAssignment.getBaseline();
        ElasticIp elasticIpBaseline = elasticIpAssignmentBaseline.getElasticIp();
        String associatedIpAddressFrom = elasticIpBaseline.getAssociatedIpAddress();
        ElasticIp elasticIp = elasticIpAssignment.getElasticIp();
        String associatedIpAddressTo = elasticIp.getAssociatedIpAddress();
        logger.info(LOGTAG + "***UpdateAction: elasticIpAddress:" + elasticIpBaseline.getElasticIpAddress() + "=>"
                + elasticIp.getElasticIpAddress());
        logger.info(LOGTAG + "associatedIpAddress:" + associatedIpAddressFrom + "=>" + associatedIpAddressTo);
        if ((associatedIpAddressFrom == null && associatedIpAddressTo == null)
                || (associatedIpAddressFrom != null && associatedIpAddressFrom.equals(associatedIpAddressTo))) {
            String errorMessage = "Provider unable to handle this situation/Nothing to be done (both associatedIpAddressFrom and  associatedIpAddressTo have no value or they are the same), therefore ignored";
            logger.warn(LOGTAG + errorMessage);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(errorMessage);
        }
        StringBuffer status = new StringBuffer(
                " created by ElasticIpService-" + getDeployEnv() + ",sendAppId=" + elasticIpAssignment.getMessageId().getSenderAppId() + ";"
                        + "updateActionStatus[staticNatProvisioning/Deprovisioning,serviceNowElasticIpRequestCreate,updateRecordToDb]:");
        try {
            // provisioning or deprovisioning failure will abort the process
            if (associatedIpAddressFrom != null) {
                processStaticNatDeprovisioning(elasticIpBaseline.getElasticIpAddress(), associatedIpAddressFrom);
                status.append("staticNatDeprovisioning=success,");
            }
            if (associatedIpAddressTo != null) {
                processStaticNatProvisioning(elasticIp.getElasticIpAddress(), associatedIpAddressTo);
                status.append("staticNatProvisioning=success,");
            }
            // serviceNowElasticIpRequestCreate failure will not abort
            if (associatedIpAddressFrom != null) {
                serviceNowElasticIpRequestCreate(elasticIpBaseline.getElasticIpAddress(), associatedIpAddressFrom);
                status.append(" serviceNowElasticIpRequestCreateForDeprovisioning=success,");
            }
            if (associatedIpAddressTo != null) {
                serviceNowElasticIpRequestCreate(elasticIp.getElasticIpAddress(), associatedIpAddressTo);
                status.append(" serviceNowElasticIpRequestCreateForProvisioning=success");
            }
            updateRecordWithRdbmsCommand(elasticIpAssignment);
            status.append("updateRecordToDb=success,");
        } catch (Throwable t) {
            logger.error(LOGTAG + status, t);
            Incident incident = serviceNowIncidentGenerate("ElasticIpAssignment.update error:" + t.getMessage() + "elasticIpAddress:"
                    + elasticIpBaseline.getElasticIpAddress() + "=>" + elasticIp.getElasticIpAddress() + ", associatedIpAddress:"
                    + associatedIpAddressFrom + "=>" + associatedIpAddressTo, t.getMessage() + ";" + status);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException("An error happend for this service:" + t.getMessage()
                    + " An ServiceNowIncident has been created with reference number:" + incident.getNumber() + ";" + status);
        } finally {
            logger.info(LOGTAG + status);
        }
    }

    private void processStaticNatProvisioning(String elasticIpAddress, String associatedIpAddressTo)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectGenerateException,
            XmlEnterpriseObjectException, EnterpriseObjectQueryException, InterruptedException, ProviderException {
        StaticNatProvisioning staticNatProvisioning = staticNatProvinsioningGenerate(elasticIpAddress, associatedIpAddressTo);
        StaticNatProvisioningQuerySpecification staticNatProvisioningQuerySpecification = (StaticNatProvisioningQuerySpecification) appConfig
                .getObjectByType(StaticNatProvisioningQuerySpecification.class.getName());
        staticNatProvisioningQuerySpecification.setProvisioningId(staticNatProvisioning.getProvisioningId());
        long startTime = System.currentTimeMillis();
        long staticNatProvisioninTimeUpTime = startTime + (long) (staticNatProvisioningMaxWaitInMinutes * 60 * 1000);
        while (System.currentTimeMillis() < staticNatProvisioninTimeUpTime
                && !"completed".equalsIgnoreCase(staticNatProvisioning.getStatus())) {
            logger.warn(LOGTAG + "CurrentStatus:" + staticNatProvisioning.getStatus() + ",provisioningResult:"
                    + staticNatProvisioning.getProvisioningResult() + ".Sleeping in seconds:" + staticNatPollingIntervalInSeconds + "...");
            Thread.sleep(staticNatPollingIntervalInSeconds * 1000);
            staticNatProvisioning = queryStaticNatProvisioning(staticNatProvisioningQuerySpecification);
        }
        if (!"success".equalsIgnoreCase(staticNatProvisioning.getProvisioningResult()))
            throw new ProviderException("staticNatProvisining failure or max time passed without success. Current Status:"
                    + staticNatProvisioning.getStatus() + ",provisioningResult:" + staticNatProvisioning.getProvisioningResult());
    }

    private void processStaticNatDeprovisioning(String elasticIpAddress, String associatedIpAddressFrom)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, XmlEnterpriseObjectException,
            EnterpriseObjectGenerateException, EnterpriseObjectQueryException, InterruptedException, ProviderException {
        StaticNatDeprovisioning staticNatDeprovisioning = staticNatDeprovisioningGenerate(elasticIpAddress, associatedIpAddressFrom);
        StaticNatDeprovisioningQuerySpecification staticNatDeprovisioningQuerySpecification = (StaticNatDeprovisioningQuerySpecification) appConfig
                .getObjectByType(StaticNatDeprovisioningQuerySpecification.class.getName());
        staticNatDeprovisioningQuerySpecification.setProvisioningId(staticNatDeprovisioning.getProvisioningId());
        long startTime = System.currentTimeMillis();
        long staticNatDeprovisioninTimeUpTime = startTime + (long) (staticNatDeprovisioningMaxWaitInMinutes * 60 * 1000);
        while (System.currentTimeMillis() < staticNatDeprovisioninTimeUpTime
                && !"completed".equalsIgnoreCase(staticNatDeprovisioning.getStatus())) {
            logger.warn(LOGTAG + "CurrentStatus:" + staticNatDeprovisioning.getStatus() + ",provisioningResult:"
                    + staticNatDeprovisioning.getProvisioningResult() + ".Sleeping in seconds:" + staticNatPollingIntervalInSeconds
                    + "...");
            Thread.sleep(staticNatPollingIntervalInSeconds * 1000);
            staticNatDeprovisioning = queryStaticNatDeprovisioning(staticNatDeprovisioningQuerySpecification);
        }
        if (!"success".equalsIgnoreCase(staticNatDeprovisioning.getProvisioningResult()))
            throw new ProviderException("staticNatDeprovisining failure or max time passed without success. Current Status:"
                    + staticNatDeprovisioning.getStatus() + ",provisioningResult:" + staticNatDeprovisioning.getProvisioningResult());
    }

    private void updateRecordWithRdbmsCommand(ElasticIpAssignment elasticIpAssignment) throws EnterpriseObjectUpdateException {
        MessageProducer elasticIpProducer = getRequestServiceMessageProducer(elasticIpServiceRequestProducerPool);
        try {
            elasticIpAssignment.setCommandName(rdbmsRequestCommand);
            elasticIpAssignment.update((RequestService) elasticIpProducer);
        } finally {
            elasticIpServiceRequestProducerPool.releaseProducer(elasticIpProducer);
        }
    }

    private StaticNatProvisioning staticNatProvinsioningGenerate(String publicIpAddress, String associatedIpAddressTo)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectGenerateException,
            XmlEnterpriseObjectException {
        StaticNatProvisioning staticNatProvisioning = (StaticNatProvisioning) appConfig
                .getObjectByType(StaticNatProvisioning.class.getName());
        StaticNat staticNat = (StaticNat) appConfig.getObjectByType(StaticNat.class.getName());
        staticNat.setPublicIp(publicIpAddress);
        staticNat.setPrivateIp(associatedIpAddressTo);
        staticNatProvisioning.setCommandName(staticNatProvisioningRequestCommand);
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        List<StaticNatProvisioning> staticNatProvisioningsReturned = new ArrayList<>();
        try {
            logger.info(LOGTAG + "StaticNatProvisioning.generate:staticNat=" + staticNat.toXmlString());
            staticNatProvisioningsReturned = staticNatProvisioning.generate(staticNat, (RequestService) networkOpsProducer);
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        logger.info(LOGTAG + "staticNatProvisioningsReturnedt.generateReturn.size=" + staticNatProvisioningsReturned.size());
        logger.info(LOGTAG + "staticNatProvisioningsReturnedt.generateReturn[0]=" + staticNatProvisioningsReturned.get(0).toXmlString());
        return staticNatProvisioningsReturned.get(0);
    }
    /**
     * 4. For the Delete-Request I believe what now has to happen is: If the
     * associated IP is not null, send a StaticNatDeprovisioning.generate to the
     * NetworkOpsService Send an ElasticIpAssignment.Delete-Request to the
     * RdbmsRequest command to delete the object in the ElasticIpService
     * database If an StaticNat was deleted in step a, send an
     * ElasticIpRequest.Create-Request ServiceNow to record that an ElasticIp
     * was deprovisioned If any of this fails send an Incident.Create-Request to
     * ServiceNow and respond to the requestor with an error
     */
    @Override
    public void delete(ElasticIpAssignment elasticIpAssignment) throws org.openeai.jms.consumer.commands.provider.ProviderException {
        ElasticIp elasticIp = elasticIpAssignment.getElasticIp();
        String elasticIpAddress = elasticIp.getElasticIpAddress();
        String associatedIpAddress = elasticIp.getAssociatedIpAddress();
        logger.info(LOGTAG + "***DeleteAction: elasticIpAddress=" + elasticIpAddress + ",associatedIpAddress=" + associatedIpAddress);
        StringBuffer status = new StringBuffer(" created by ElasticIpService-" + getDeployEnv() + ",sendAppId="
                + elasticIpAssignment.getMessageId().getSenderAppId() + ";"
                + "deleteActionStatus[staticNatDeprovisioning,serviceNowElasticIpRequestCreateForDeprovisioning,deleteRecordToDb,]:");
        try {
            status.append("associatedIpAddress=" + associatedIpAddress);
            if (associatedIpAddress != null) {
                processStaticNatDeprovisioning(elasticIpAddress, associatedIpAddress);
                status.append("staticNatDeprovisioning=success,");
                Result result = serviceNowElasticIpRequestCreate(elasticIpAddress, associatedIpAddress);
                status.append(String.format(" serviceNowElasticIpRequestCreateForDeprovisioning=s%,", result.getStatus()));
                // if (!"success".equals(result.getStatus().toLowerCase()))
                // throw new RuntimeException(result.toXmlString());
            }
            // per Tod's request, user need to delete the assignment even when
            // there is no associatedIpAddress
            // also only delete it if deprovisioning succeed
            deleteRecordWithRdmsCommand(elasticIpAssignment);
            status.append("deleteRecordToDb=success,");
        } catch (Throwable t) {
            logger.error(LOGTAG + status, t);
            /**
             * <IncidentRequisition> <ShortDescription>More account distribution
             * lists needed for account series aws-test</ShortDescription>
             * <Description>Please provision more accounts for the aws-test
             * account series. Only 10 unused distribution lists
             * remain.</Description> <Urgency>3</Urgency> <Impact>3</Impact>
             * <BusinessService>Application Management</BusinessService>
             * <Category>Configuration</Category> <SubCategory>Add</SubCategory>
             * <RecordType>Service Request</RecordType>
             * <ContactType>Integration</ContactType> <CallerId>guest</CallerId>
             * <CmdbCi>Emory AWS Service</CmdbCi> <AssignmentGroup>LITS:
             * Messaging - Tier 3</AssignmentGroup> </IncidentRequisition>
             */
            serviceNowIncidentGenerate("ElasticIpAssignment.delete error:" + "elasticIpAddress=" + elasticIpAddress
                    + ",associatedIpAddress=" + associatedIpAddress, t.getMessage() + ";" + status);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(t.getMessage());
        } finally {
            logger.info(LOGTAG + status);
        }
    }

    private void deleteRecordWithRdmsCommand(ElasticIpAssignment elasticIpAssignment) throws EnterpriseObjectDeleteException {
        MessageProducer elasticIpProducer = getRequestServiceMessageProducer(elasticIpServiceRequestProducerPool);
        try {
            elasticIpAssignment.setCommandName(rdbmsRequestCommand);
            elasticIpAssignment.delete("delete", (RequestService) elasticIpProducer);
        } finally {
            elasticIpServiceRequestProducerPool.releaseProducer(elasticIpProducer);
        }
    }

    // private Incident serviceNowIncidentGenerate(String shorDescription,
    // Throwable t, String senderAppId)
    // throws org.openeai.jms.consumer.commands.provider.ProviderException {
    // List<Incident> generatedIncidents = null;
    // try {
    // Incident incident = (Incident)
    // appConfig.getObjectByType(Incident.class.getName());
    // IncidentRequisition incidentRequisition = (IncidentRequisition)
    // appConfig.getObjectByType(IncidentRequisition.class.getName());
    // incidentRequisition.setShortDescription(shorDescription);
    // incidentRequisition
    // .setDescription(t.getMessage() + ",created by ElasticIpService-" +
    // getDeployEnv() + ",sendAppId=" + senderAppId);
    // incidentRequisition.setUrgency("3");
    // incidentRequisition.setImpact("3");
    // incidentRequisition.setBusinessService("Application Management");
    // incidentRequisition.setCategory("Configuration");
    // incidentRequisition.setSubCategory("Add");
    // incidentRequisition.setRecordType("Service Request");
    // incidentRequisition.setContactType("Integration");
    // incidentRequisition.setCallerId(incidentRequisitionCallerId);
    // incidentRequisition.setCmdbCi("Emory AWS Service");
    // incidentRequisition.setAssignmentGroup("LITS: Messaging - Tier 3");
    //
    // logger.info("incident.generate:incidentRequisition=" +
    // incidentRequisition.toXmlString());
    // MessageProducer serviceNowProducer =
    // getRequestServiceMessageProducer(serviceNowServiceRequestProducerPool);
    // try {
    // generatedIncidents = incident.generate(incidentRequisition,
    // (RequestService) serviceNowProducer);
    // } finally {
    // serviceNowServiceRequestProducerPool.releaseProducer(serviceNowProducer);
    // }
    // } catch (Throwable e1) {
    // logger.error(LOGTAG, e1);
    // throw new
    // org.openeai.jms.consumer.commands.provider.ProviderException("ServiceNowIncident.Generate
    // error:" + e1.getMessage());
    // }
    // return generatedIncidents.get(0);
    // }

    private Incident serviceNowIncidentGenerate(String shorDescription, String description)
            throws org.openeai.jms.consumer.commands.provider.ProviderException {
        List<Incident> generatedIncidents = null;
        try {
            Incident incident = (Incident) appConfig.getObjectByType(Incident.class.getName());
            IncidentRequisition incidentRequisition = (IncidentRequisition) appConfig.getObjectByType(IncidentRequisition.class.getName());
            incidentRequisition.setShortDescription(shorDescription);
            incidentRequisition.setDescription(description);
            incidentRequisition.setUrgency("3");
            incidentRequisition.setImpact("3");
            incidentRequisition.setBusinessService("Application Management");
            incidentRequisition.setCategory("Configuration");
            incidentRequisition.setSubCategory("Add");
            incidentRequisition.setRecordType("Service Request");
            incidentRequisition.setContactType("Integration");
            incidentRequisition.setCallerId(incidentRequisitionCallerId);
            incidentRequisition.setCmdbCi("Emory AWS Service");
            incidentRequisition.setAssignmentGroup("LITS: Network Operations Center - Run");

            logger.info("incident.generate:incidentRequisition=" + incidentRequisition.toXmlString());
            MessageProducer serviceNowProducer = getRequestServiceMessageProducer(serviceNowServiceRequestProducerPool);
            try {
                generatedIncidents = incident.generate(incidentRequisition, (RequestService) serviceNowProducer);
            } finally {
                serviceNowServiceRequestProducerPool.releaseProducer(serviceNowProducer);
            }
        } catch (Throwable e1) {
            logger.error(LOGTAG, e1);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException("ServiceNowIncident.Generate error:" + e1.getMessage());
        }
        return generatedIncidents.get(0);
    }

    private Result serviceNowElasticIpRequestCreate(String elasticIpAddress, String associatedIpAddress)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectCreateException,
            XmlEnterpriseObjectException {
        // <!ELEMENT ElasticIpRequest (SystemId?, ElasticIpAddress,
        // PrivateIpAddress, UserNetID, RequestNumber?, Domain, RequestState?,
        // RequestItemNumber?, RequestItemState?)>
        ElasticIpRequest elasticIpRequest = (ElasticIpRequest) appConfig.getObjectByType(ElasticIpRequest.class.getName());
        elasticIpRequest.setElasticIpAddress(elasticIpAddress);
        elasticIpRequest.setPrivateIpAddress(associatedIpAddress);
        elasticIpRequest.setUserNetID(elasticIpRequestUserNetID);
        elasticIpRequest.setDomain("emory.edu");
        logger.info(LOGTAG + "elasticIpRequest.create=" + elasticIpRequest.toXmlString());
        MessageProducer serviceNowProducer = getRequestServiceMessageProducer(serviceNowServiceRequestProducerPool);
        XmlEnterpriseObject createReturn = null;
        try {
            createReturn = elasticIpRequest.create((RequestService) serviceNowProducer);
        } catch (Exception e) {
            logger.info(LOGTAG, e);
        } finally {
            serviceNowServiceRequestProducerPool.releaseProducer(serviceNowProducer);
        }
        logger.info(LOGTAG + "elasticIpRequest.create.returned=" + createReturn.toXmlString());
        return (Result) createReturn;
    }

    private StaticNatDeprovisioning staticNatDeprovisioningGenerate(String elasticIpAddress, String associatedIpAddress)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, XmlEnterpriseObjectException,
            EnterpriseObjectGenerateException {
        StaticNatDeprovisioning staticNatDeprovisioningTemplate = (StaticNatDeprovisioning) appConfig
                .getObjectByType(StaticNatDeprovisioning.class.getName());
        StaticNat staticNat = (StaticNat) appConfig.getObjectByType(StaticNat.class.getName());
        staticNat.setPublicIp(elasticIpAddress);
        staticNat.setPrivateIp(associatedIpAddress);
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        staticNatDeprovisioningTemplate.setCommandName(staticNatDeprovisioningRequestCommand);
        List<StaticNatDeprovisioning> staticNatDeprovisioningsReturned = new ArrayList<>();
        try {
            staticNatDeprovisioningsReturned = staticNatDeprovisioningTemplate.generate(staticNat, (RequestService) networkOpsProducer);
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        logger.info(LOGTAG + "staticNatDeprovisioning.generateReturn.size=" + staticNatDeprovisioningsReturned.size());
        logger.info(LOGTAG + "staticNatDeprovisioning.generateReturn[0]=" + staticNatDeprovisioningsReturned.get(0).toXmlString());
        return staticNatDeprovisioningsReturned.get(0);
    }

    /**
     * @throws org.openeai.jms.consumer.commands.provider.ProviderException
     * @see ElasticIpAssignmentProvider.java
     */
    @Override
    public ElasticIpAssignment generate(ElasticIpRequisition req) throws org.openeai.jms.consumer.commands.provider.ProviderException {
        ElasticIpAssignment elasticIpAssignment = new ElasticIpAssignment();
        List<XmlEnterpriseObject> elasticIpsUnassignedReturned = null;
        List<XmlEnterpriseObject> elasticIpAssignmentsReturned = null;
        try {
            logger.info(LOGTAG + "***GenerateAction: ElasticIpRequisition=" + req.toXmlString());
            elasticIpAssignment = (ElasticIpAssignment) appConfig.getObjectByType(ElasticIpAssignment.class.getName());
            ElasticIp elasticIp = (ElasticIp) appConfig.getObjectByType(ElasticIp.class.getName());
            ElasticIpQuerySpecification elasticIpQuerySpecification = (ElasticIpQuerySpecification) appConfig
                    .getObjectByType(ElasticIpQuerySpecification.class.getName());
            elasticIpQuerySpecification.setName(QUERY_LANGUAGE_FIND_UNASSIGNED);
            MessageProducer elasticIpProducer = getRequestServiceMessageProducer(elasticIpServiceRequestProducerPool);
            try {
                if (verbose) {
                    logger.info(LOGTAG + "moaIn=" + elasticIpQuerySpecification.toXmlString());
                    logger.info(LOGTAG + "elasticIp.getCommandName=" + elasticIp.getCommandName());
                }
                elasticIpsUnassignedReturned = OpeneaiCommand.QueryCommand().exec(elasticIp, (RequestService) elasticIpProducer,
                        elasticIpQuerySpecification);
                logger.info(
                        LOGTAG + "Received response. There are " + elasticIpsUnassignedReturned.size() + " available elasticIps found.");
                if (elasticIpsUnassignedReturned.isEmpty())
                    throw new ProviderException("Error generating ElasticIpAssignment - 0 availale ElasticIp found");
                elasticIpAssignment.setCommandName(rdbmsRequestCommand);
                assignAndPersist(req, elasticIpAssignment, elasticIpsUnassignedReturned, elasticIpProducer);
                if (elasticIpAssignment.getElasticIp() == null)
                    throw new ProviderException(
                            "Error generating ElasticIpAssignment - no availale ElasticIp: elasticIpsUnassignedReturned= "
                                    + elasticIpsUnassignedReturned.size() + " but used up by another thread: should be very rare");
                elasticIpAssignmentsReturned = queryJustCreated(elasticIpAssignment, elasticIpProducer);
                if (elasticIpAssignmentsReturned == null || elasticIpAssignmentsReturned.isEmpty())
                    throw new ProviderException(
                            "Error generating ElasticIpAssignment: Generation successful, but was immediately deleted by others (should not happen)? ");
            } finally {
                try {
                    elasticIpServiceRequestProducerPool.releaseProducer(elasticIpProducer);
                } catch (Throwable t) {
                }
            }
        } catch (Exception e) {
            logger.error("Error generating ElasticIpAssignment:", e);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(e.getMessage());
        }
        return (ElasticIpAssignment) elasticIpAssignmentsReturned.get(0);
    }
    private List<XmlEnterpriseObject> queryJustCreated(ElasticIpAssignment elasticIpAssignment, MessageProducer producer)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectQueryException,
            EnterpriseObjectGenerateException {
        List<XmlEnterpriseObject> elasticIpAssignmentsReturned;
        ElasticIpAssignmentQuerySpecification elasticIpAssignmentQuerySpecification = (ElasticIpAssignmentQuerySpecification) appConfig
                .getObjectByType(ElasticIpAssignmentQuerySpecification.class.getName());
        elasticIpAssignmentQuerySpecification.setElasticIpId(elasticIpAssignment.getElasticIp().getElasticIpId());
        logger.info("querying elasticIpAssignment with elasticIpId:" + elasticIpAssignmentQuerySpecification.getElasticIpId());
        elasticIpAssignmentsReturned = OpeneaiCommand.QueryCommand().exec(elasticIpAssignment, (RequestService) producer,
                elasticIpAssignmentQuerySpecification);
        return elasticIpAssignmentsReturned;
    }

    private StaticNatProvisioning queryStaticNatProvisioning(
            StaticNatProvisioningQuerySpecification staticNatProvisioningQuerySpecification) throws EnterpriseConfigurationObjectException,
            EnterpriseFieldException, EnterpriseObjectQueryException, EnterpriseObjectGenerateException {
        List<XmlEnterpriseObject> staticNatProvisioningsReturned;
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        try {
            logger.info(
                    "querying staticNatProvisioning with provisioningId:" + staticNatProvisioningQuerySpecification.getProvisioningId());
            staticNatProvisioningsReturned = OpeneaiCommand.QueryCommand().exec(staticNatProvisioningTemplate,
                    (RequestService) networkOpsProducer, staticNatProvisioningQuerySpecification);
            logger.info("statusReturned:" + ((StaticNatProvisioning) staticNatProvisioningsReturned.get(0)).getStatus());
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        return (StaticNatProvisioning) staticNatProvisioningsReturned.get(0);
    }

    private StaticNatDeprovisioning queryStaticNatDeprovisioning(
            StaticNatDeprovisioningQuerySpecification staticNatDeprovisioningQuerySpecification)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException, EnterpriseObjectQueryException,
            EnterpriseObjectGenerateException {
        List<XmlEnterpriseObject> staticNatDeprovisioningsReturned;
        MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
        try {
            logger.info("querying staticNatDeprovisioning with provisioningId:"
                    + staticNatDeprovisioningQuerySpecification.getProvisioningId());
            staticNatDeprovisioningsReturned = OpeneaiCommand.QueryCommand().exec(staticNatDeprovisioningTemplate,
                    (RequestService) networkOpsProducer, staticNatDeprovisioningQuerySpecification);
            logger.info("statusReturned:" + ((StaticNatDeprovisioning) staticNatDeprovisioningsReturned.get(0)).getStatus());
        } finally {
            networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
        }
        return (StaticNatDeprovisioning) staticNatDeprovisioningsReturned.get(0);
    }
    private void assignAndPersist(ElasticIpRequisition req, ElasticIpAssignment elasticIpAssignment,
            List<XmlEnterpriseObject> elasticIpsUnassignedReturned, MessageProducer producer)
            throws EnterpriseFieldException, EnterpriseObjectCreateException, EnterpriseObjectDeleteException,
            EnterpriseObjectUpdateException, XmlEnterpriseObjectException {
        for (XmlEnterpriseObject e : elasticIpsUnassignedReturned) {
            ElasticIp elasticIpReturned = (ElasticIp) e;
            elasticIpAssignment.setElasticIp(elasticIpReturned);
            elasticIpAssignment.setOwnerId(req.getOwnerId());
            elasticIpAssignment.setDecription("System/Generate operation");
            elasticIpAssignment.setPurpose("System/Generate operation");
            elasticIpAssignment.setCreateUser("System/Generate operation");
            elasticIpAssignment.setCreateDatetime(new Datetime("Create", System.currentTimeMillis()));

            logger.info(LOGTAG + "Creating with ElasticIpAssignment.CreateRequest =" + elasticIpAssignment.toXmlString());
            XmlEnterpriseObject resultReturned = OpeneaiCommand.CREATECOMMAND.exec((RequestService) producer, elasticIpAssignment);
            logger.info(LOGTAG + "ElasticIpAssignment.CreateRequest returned=" + resultReturned.toXmlString());
            Result result = (Result) resultReturned;
            if ("success".equalsIgnoreCase(result.getStatus()))
                break;
        }
    }

    /**
     * proxy to delegate to rdbmsRequestCommand
     *
     * @see org.openeai.jms.consumer.commands.provider.AbstractCompleteProvider#
     *      create(java.lang.Object)
     */
    @Override
    public void create(ElasticIpAssignment m) throws org.openeai.jms.consumer.commands.provider.ProviderException {
        MessageProducer elasticIpProducer = getRequestServiceMessageProducer(elasticIpServiceRequestProducerPool);
        try {
            m.setCommandName(rdbmsRequestCommand);
            logger.info("***CreateAction: elasticIpAssignment :" + m.toXmlString());
            OpeneaiCommand.CREATECOMMAND.exec((RequestService) elasticIpProducer, m);
        } catch (EnterpriseObjectCreateException | EnterpriseObjectDeleteException | EnterpriseObjectUpdateException
                | XmlEnterpriseObjectException e) {
            logger.error(LOGTAG, e);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(e.getMessage());
        } finally {
            try {
                elasticIpServiceRequestProducerPool.releaseProducer(elasticIpProducer);
            } catch (Throwable t) {
            }
        }
    }

    /**
     * proxy to delegate to rdbmsRequestCommand
     *
     * @see org.openeai.jms.consumer.commands.provider.AbstractCompleteProvider#query
     *      (java.lang.Object)
     */
    @Override
    public List<ElasticIpAssignment> query(ElasticIpAssignmentQuerySpecification elasticIpAssignmentQuerySpecification)
            throws org.openeai.jms.consumer.commands.provider.ProviderException {
        List<ElasticIpAssignment> elasticIpAssignments = new ArrayList<>();
        ElasticIpAssignment elasticIpAssignment = null;
        MessageProducer elasticIpProducer = getRequestServiceMessageProducer(elasticIpServiceRequestProducerPool);
        try {
            elasticIpAssignment = (ElasticIpAssignment) appConfig.getObjectByType(ElasticIpAssignment.class.getName());
            List<XmlEnterpriseObject> elasticIpAssignmentsReturned;
            logger.info("***QueryAction: elasticIpAssignmentQuerySpecification :" + elasticIpAssignmentQuerySpecification.toXmlString());
            elasticIpAssignment.setCommandName(rdbmsRequestCommand);
            elasticIpAssignmentsReturned = OpeneaiCommand.QueryCommand().exec(elasticIpAssignment, (RequestService) elasticIpProducer,
                    elasticIpAssignmentQuerySpecification);
            for (XmlEnterpriseObject x : elasticIpAssignmentsReturned)
                elasticIpAssignments.add((ElasticIpAssignment) x);
        } catch (EnterpriseObjectQueryException | EnterpriseObjectGenerateException | EnterpriseConfigurationObjectException
                | XmlEnterpriseObjectException e) {
            logger.error(LOGTAG, e);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(e.getMessage());
        } finally {
            try {
                elasticIpServiceRequestProducerPool.releaseProducer(elasticIpProducer);
            } catch (Throwable t) {
            }
        }
        // here for testing, really useful for update and delete actions
        StringBuffer status = new StringBuffer("queryActionStatus:");

        status.append(" created by ElasticIpService-" + getDeployEnv() + ":" + elasticIpAssignments.size() + " elements found");
        if (verbose)
            logger.info(LOGTAG + status);
        return elasticIpAssignments;
    }
}
