/*******************************************************************************
 $Source: $
 $Revision: $
 *******************************************************************************/

/******************************************************************************

 Copyright (C) 2018 Emory University. All rights reserved.
 ******************************************************************************/

package edu.emory.elasticip.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.openeai.afa.ScheduledCommand;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.consumer.commands.provider.ProviderException;
import org.openeai.jms.producer.MessageProducer;
import org.openeai.moa.EnterpriseObjectCreateException;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.RequestService;
import org.openeai.utils.webservice.command.OpeneaiCommand;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountNotification;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloud;
import com.amazon.aws.moa.objects.resources.v1_0.Datetime;
import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudQuerySpecification;

import edu.emory.moa.jmsobjects.network.v1_0.ElasticIpAssignment;
import edu.emory.moa.jmsobjects.network.v1_0.StaticNat;
import edu.emory.moa.objects.resources.v1_0.ElasticIpAssignmentQuerySpecification;
import edu.emory.moa.objects.resources.v1_0.StaticNatQuerySpecification;

/**
 *TODO: to be deleted? moved to NetworkOpsService
 *
 * @author George Wang (gwang28@emory.edu)
 * @version 1.0 - 4 July 2018
 *
 */
public class ElasticIpRegistryScheduledCommand extends AScheduledCommand implements ScheduledCommand {
    private String LOGTAG = "[ElasticIpRegistryScheduledCommand] ";
    protected static final Logger logger = LogManager.getLogger(ElasticIpRegistryScheduledCommand.class);

    public ElasticIpRegistryScheduledCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + " Initializing ...");

    }
    private List<ElasticIpAssignment> queryAllElasticIpAssignment() throws org.openeai.jms.consumer.commands.provider.ProviderException {
        List<ElasticIpAssignment> elasticIpAssignments = new ArrayList<>();

        MessageProducer elasticIpProducer = getRequestServiceMessageProducer(elasticIpServiceRequestProducerPool);
        try {
            ElasticIpAssignmentQuerySpecification elasticIpAssignmentQuerySpecificatio = (ElasticIpAssignmentQuerySpecification) getAppConfig()
                    .getObjectByType(ElasticIpAssignmentQuerySpecification.class.getName());
            ElasticIpAssignment elasticIpAssignment = (ElasticIpAssignment) getAppConfig()
                    .getObjectByType(ElasticIpAssignment.class.getName());
            List<XmlEnterpriseObject> elasticIpAssignmentsReturned;
            elasticIpAssignment.setCommandName(rdbmsRequestCommand);
            elasticIpAssignmentsReturned = OpeneaiCommand.QueryCommand().exec(elasticIpAssignment, (RequestService) elasticIpProducer,
                    elasticIpAssignmentQuerySpecificatio);
            for (XmlEnterpriseObject x : elasticIpAssignmentsReturned)
                elasticIpAssignments.add((ElasticIpAssignment) x);
        } catch (EnterpriseObjectQueryException | EnterpriseObjectGenerateException | EnterpriseConfigurationObjectException e) {
            logger.error(LOGTAG, e);
            throw new org.openeai.jms.consumer.commands.provider.ProviderException(e.getMessage());
        } finally {
            try {
                elasticIpServiceRequestProducerPool.releaseProducer(elasticIpProducer);
            } catch (Throwable t) {
            }
        }
        return elasticIpAssignments;
    }

    @Override
    public int execute() throws ScheduledCommandException {
        long start = System.currentTimeMillis();
        logger.info(LOGTAG + "Executing ...");
        try {
            StaticNatQuerySpecification staticNatQuerySpecification = (StaticNatQuerySpecification) getAppConfig()
                    .getObjectByType(StaticNatQuerySpecification.class.getName());
            List<ElasticIpAssignment> elasticIpAssignments = queryAllElasticIpAssignment();
            logger.info(LOGTAG + "elasticIpAssignments.size()=" + elasticIpAssignments.size());
            for (ElasticIpAssignment elasticIpAssignment : elasticIpAssignments) {
                staticNatQuerySpecification.setPublicIp(elasticIpAssignment.getElasticIp().getElasticIpAddress());
                MessageProducer networkOpsProducer = getRequestServiceMessageProducer(networkOpsServiceRequestProducerPool);
                List<StaticNat> staticNats = null;
                try {
                    staticNatTemplate.setCommandName(rdbmsRequestCommand);
                    staticNats = staticNatTemplate.query(staticNatQuerySpecification, (RequestService) networkOpsProducer);
                    logger.info(LOGTAG + "elasticIpAssignment..elasticIp.elasticIpAddress="
                            + elasticIpAssignment.getElasticIp().getElasticIpAddress());
                    logger.info(LOGTAG + "staticNatFound=" + staticNats == null || staticNats.isEmpty() ? null
                            : staticNats.get(0).toXmlString());
                } finally {
                    networkOpsServiceRequestProducerPool.releaseProducer(networkOpsProducer);
                }
                if (staticNats == null || staticNats.isEmpty()) {
                    String accountId = queryVpcAccountIdFromAwsAccountService(elasticIpAssignment.getOwnerId());
                    logger.info(LOGTAG + "accountIdFromAwsAccountService=" + accountId);
                    AccountNotification accountNotification = newAccountNotification(accountId);
                    String text = "StaticNat Provisioning:" + elasticIpAssignment.getElasticIp().getElasticIpAddress() + "=>"
                            + elasticIpAssignment.getElasticIp().getAssociatedIpAddress();
                    logger.info(LOGTAG + "staticNatProvisioning...");
                    if (processStaticNatProvisioning(elasticIpAssignment.getElasticIp().getElasticIpAddress(),
                            elasticIpAssignment.getElasticIp().getAssociatedIpAddress())) {
                        text = text + ":success";
                    } else {
                        text = text + ":failure";
                        serviceNowIncidentGenerate(text, "failed");
                    }
                    accountNotification.setText(text);
                    MessageProducer awsAccountProducer = getRequestServiceMessageProducer(awsAccountServiceRequestProducerPool);
                    accountNotification.setCommandName(rdbmsRequestCommand);
                    try {
                        accountNotification.create((RequestService) awsAccountProducer);
                    } finally {
                        awsAccountServiceRequestProducerPool.releaseProducer(awsAccountProducer);
                    }
                }
            }
        } catch (ProviderException | EnterpriseConfigurationObjectException | EnterpriseFieldException | EnterpriseObjectQueryException
                | EnterpriseObjectGenerateException | XmlEnterpriseObjectException | InterruptedException
                | EnterpriseObjectCreateException e) {
            logger.info(LOGTAG, e);
        }
        return 0;
    }
    private AccountNotification newAccountNotification(String accountId)
            throws EnterpriseConfigurationObjectException, EnterpriseFieldException {
        AccountNotification AccountNotification = (AccountNotification) getAppConfig().getObjectByType(AccountNotification.class.getName());
        AccountNotification.setAccountId(accountId);
        AccountNotification.setType("ElasticIp");
        AccountNotification.setSubject("Missing StaticNat");
        AccountNotification.setCreateUser("ElasticIpService");
        AccountNotification.setCreateDatetime(new Datetime());
        return AccountNotification;
    }

}
