package edu.emory.elasticip.service;

import java.io.Serializable;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.metadata.ClassMetadata;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import com.openii.openeai.toolkit.rdbms.persistence.PersistenceException;
import com.openii.openeai.toolkit.rdbms.persistence.hibernate.SessionFactoryUtil;

import edu.emory.moa.jmsobjects.network.v1_0.ElasticIp;
import edu.emory.moa.objects.resources.v1_0.Datetime;
import java.util.GregorianCalendar;

/**
 *
 * used to import into ElasticIp table for production data
 *
 * @author gwang28
 *
 */
public class ElasticIpInsertion {
    final static Logger LOG = LogManager.getLogger(ElasticIpInsertion.class);
    public ElasticIpInsertion() throws PersistenceException, EnterpriseFieldException, EnterpriseConfigurationObjectException {
        System.setProperty("docUriBase", "https://dev-config.app.emory.edu/");
        AppConfig appConfig = new AppConfig("/Users/gwang28/project/itarch/emory-elasticip-service/src/test/resources/ElasticIpService.xml",
                "edu.emory.ElasticIpService");
        ClassMetadata classMetaData = SessionFactoryUtil.getInstance().getClassMetadata(ElasticIp.class);
        if (classMetaData == null) {
            String error = " Mapping not found for " + ElasticIp.class.getName();
            LOG.fatal(error);
            throw new PersistenceException(error);
        }

        int id = 0;
        String prefix = "170.140.248.";

        for (int i = 0; i <= 255; i++) {
            ElasticIp elasticIp = (ElasticIp) appConfig.getObjectByType(ElasticIp.class.getName());
            if (i == 11 || i == 20)
                continue;
            elasticIp.setElasticIpId(String.valueOf(id));
            id++;
            elasticIp.setElasticIpAddress(prefix + i);
            elasticIp.setCreateUser("System");
            Datetime dt = elasticIp.newCreateDatetime();
            dt.update(new GregorianCalendar());
            elasticIp.setCreateDatetime(dt);
            create(elasticIp);
        }

        prefix = "170.140.249.";
        for (int i = 0; i <= 255; i++) {
            ElasticIp elasticIp = (ElasticIp) appConfig.getObjectByType(ElasticIp.class.getName());
            elasticIp.setElasticIpId(String.valueOf(id));
            id++;
            elasticIp.setElasticIpAddress(prefix + i);
            elasticIp.setCreateUser("System");
            Datetime dt = elasticIp.newCreateDatetime();
            dt.update(new GregorianCalendar());
            elasticIp.setCreateDatetime(dt);
            create(elasticIp);
        }
    }

    private static Serializable create(ElasticIp h) {
        Transaction tx = null;
        Session session = SessionFactoryUtil.getInstance().getCurrentSession();
        Serializable id = null;
        try {
            tx = session.beginTransaction();

            id = session.save(h);
            tx.commit();
        } catch (RuntimeException e) {
            e.printStackTrace();
            if (tx != null && tx.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tx.rollback();
                } catch (HibernateException e1) {
                    LOG.debug("Error rolling back transaction");
                }
                // throw again the first exception
                throw e;
            }
        }
        return id;
    }

    private static void alternate() {
        /*
        System.setProperty("openeaiClasspathFirst", "true");
        AppConfig appConfig = new AppConfig("...../ElasticIpServiceAMQ.xml", "org.rhedcloud.ElasticIpService");

        ElasticIp elasticIp = (ElasticIp) appConfig.getObjectByType(ElasticIp.class.getName());
        elasticIp.setElasticIpId("1");
        elasticIp.setElasticIpAddress("170.140.248.0");
        elasticIp.setCreateUser("System");
        Datetime dt = elasticIp.newCreateDatetime();
        dt.update(new GregorianCalendar());
        elasticIp.setCreateDatetime(dt);
        create(elasticIp);
        */
    }

    public static void main(String[] args) throws PersistenceException, EnterpriseFieldException, EnterpriseConfigurationObjectException {
        new ElasticIpInsertion();
    }
}
